(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

var _navigation = require("./modules/navigation");

},{"./modules/navigation":2}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var NavigationToggle = exports.NavigationToggle = function NavigationToggle() {
  var trigger = document.getElementById('toggleNav'),
      navbar = document.getElementById('navbar');

  trigger.addEventListener('click', function () {
    navbar.classList.toggle('show');
  });
};

var changeOnScroll = exports.changeOnScroll = function changeOnScroll() {
  var d = document;
  var win = window;
  win.addEventListener('scroll', function () {
    var el = d.getElementById('main-navigation'),
        scrollActual = d.documentElement.scrollTop;

    if (scrollActual > 300) {
      el.classList.add('on-scroll');
    } else {
      el.classList.remove('on-scroll');
    }
  });
};

},{}]},{},[1]);

//# sourceMappingURL=scripts.js.map
