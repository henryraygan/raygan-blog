export const NavigationToggle = () => {
  const trigger = document.getElementById('toggleNav'),
    navbar = document.getElementById('navbar')

  trigger.addEventListener('click', () => {
    navbar.classList.toggle('show');
  })
}


export const changeOnScroll = () => {
	const d = document;
	const win = window;
	win.addEventListener('scroll', () => {
		const el = d.getElementById('main-navigation'),
					scrollActual = d.documentElement.scrollTop

    if(scrollActual > 300) {
      el.classList.add('on-scroll');
    } else {
      el.classList.remove('on-scroll');
    }

	});
}

