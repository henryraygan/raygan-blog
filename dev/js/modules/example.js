// Este es un ejemplo de como exportar funciones desde un archivo
// En index.js se importan estas funciones

export const hello = () => {
  console.log('Hola mundo ...')
};

export const bye = () => {
  console.log('Adiós mundo ...')
};
