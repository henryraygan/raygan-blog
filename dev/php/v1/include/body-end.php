


<footer class="raygan-container">
    <section class="main-footer">
        <p class="text-center margin-bottom-1 margin-top-1" style="font-size: 14px;">
            © Henry Ramirez - Raygan 2018
        </p>
        <ul class="social">
            <li class="social-item">
                <a class="social-link" href=""><i class="fab fa-instagram"></i></a>
            </li> 
            <li class="social-item">
                <a class="social-link" href=""><i class="fab fa-linkedin-in"></i></a>
            </li> 
            <li class="social-item">
                <a class="social-link" href=""><i class="fab fa-dribbble"></i></a>
            </li> 
            <li class="social-item">
                <a class="social-link" href=""><i class="fab fa-codepen"></i></a>
            </li> 
        </ul>
    </section>
</footer>

<script src="./assets/js/scripts.js"></script>

</body>
</html>