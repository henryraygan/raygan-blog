<?php include_once "../include/head.php" ?>

<?php include_once "../components/navbar.php" ?>

<main class="raygan-container">

  <section class="raygan">
    <article class="raygan-item">
      <div class="raygan-thumb">
        <a href="">
          <img src="/assets/images/project_1x.jpg" alt="">
        </a>
      </div>
      <div class="raygan-detail">
        <h2 class="raygan-detail-title">
          <a href="">Lorem ipsum dolor sit amet consectetur.</a>
        </h2>
        <p class="raygan-detail-abstract">
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Qui dolor eos animi eius quo veniam?
        </p>
      </div>
    </article>
    <article class="raygan-item">
      <div class="raygan-thumb">
        <a href="">
          <img src="/assets/images/project_1x.jpg" alt="">
        </a>
      </div>
      <div class="raygan-detail">
        <h2 class="raygan-detail-title">
          <a href="">Lorem ipsum dolor sit amet consectetur.</a>
        </h2>
        <p class="raygan-detail-abstract">
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Qui dolor eos animi eius quo veniam?
        </p>
      </div>
    </article>
    <article class="raygan-item">
      <div class="raygan-thumb">
        <a href="">
          <img src="/assets/images/project_1x.jpg" alt="">
        </a>
      </div>
      <div class="raygan-detail">
        <h2 class="raygan-detail-title">
          <a href="">Lorem ipsum dolor sit amet consectetur.</a>
        </h2>
        <p class="raygan-detail-abstract">
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Qui dolor eos animi eius quo veniam?
        </p>
      </div>
    </article>
    <article class="raygan-item">
      <div class="raygan-thumb">
        <a href="">
          <img src="/assets/images/project_1x.jpg" alt="">
        </a>
      </div>
      <div class="raygan-detail">
        <h2 class="raygan-detail-title">
          <a href="">Lorem ipsum dolor sit amet consectetur.</a>
        </h2>
        <p class="raygan-detail-abstract">
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Qui dolor eos animi eius quo veniam?
        </p>
      </div>
    </article>
    <article class="raygan-item">
      <div class="raygan-thumb">
        <a href="">
          <img src="/assets/images/project_1x.jpg" alt="">
        </a>
      </div>
      <div class="raygan-detail">
        <h2 class="raygan-detail-title">
          <a href="">Lorem ipsum dolor sit amet consectetur.</a>
        </h2>
        <p class="raygan-detail-abstract">
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Qui dolor eos animi eius quo veniam?
        </p>
      </div>
    </article>
    <article class="raygan-item">
      <div class="raygan-thumb">
        <a href="">
          <img src="/assets/images/project_1x.jpg" alt="">
        </a>
      </div>
      <div class="raygan-detail">
        <h2 class="raygan-detail-title">
          <a href="">Lorem ipsum dolor sit amet consectetur.</a>
        </h2>
        <p class="raygan-detail-abstract">
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Qui dolor eos animi eius quo veniam?
        </p>
      </div>
    </article>
    <article class="raygan-item">
      <div class="raygan-thumb">
        <a href="">
          <img src="/assets/images/project_1x.jpg" alt="">
        </a>
      </div>
      <div class="raygan-detail">
        <h2 class="raygan-detail-title">
          <a href="">Lorem ipsum dolor sit amet consectetur.</a>
        </h2>
        <p class="raygan-detail-abstract">
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Qui dolor eos animi eius quo veniam?
        </p>
      </div>
    </article>
    <article class="raygan-item">
      <div class="raygan-thumb">
        <a href="">
          <img src="/assets/images/project_1x.jpg" alt="">
        </a>
      </div>
      <div class="raygan-detail">
        <h2 class="raygan-detail-title">
          <a href="">Lorem ipsum dolor sit amet consectetur.</a>
        </h2>
        <p class="raygan-detail-abstract">
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Qui dolor eos animi eius quo veniam?
        </p>
      </div>
    </article>
  </section>

</main>



<?php include_once "../include/body-end.php" ?>
