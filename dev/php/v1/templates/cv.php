<?php include_once "../include/head.php" ?>

<?php include_once "../components/navbar.php" ?>

<main class="raygan-container">

  <article class="cv">

    <img class="main-photo-profile" src="/assets/images/photo-raygan.jpg" alt="">

    <div class="raygan-container">
      <section class="cv-description">
        <h2 class="cv-title">
          Hola
        </h2>
        <p class="cv-about">
          
        </p>
      </section>

      <section class="cv-skills">
        <div class="cv-skills-item">
          <h3 class="cv-skills-title">
            Frontend
          </h3>
          <div class="cv-skills-description">
            <ul class="skillset">
              <li class="skillset-item">
                HTML5
              </li>
            </ul>
            CSS3
            <li class="skillset-item">
              SASS
            </li> 
            
            <li class="skillset-item">
              BEM
            </li> 
            
            <li class="skillset-item">
              SMACSS
            </li>
            
            <li class="skillset-item">
              OCSS
            </li>
            <li class="skillset-item">
              Animations
            </li>
            <li class="skillset-item">
              Gulp
            </li>
          </div>
        </div>
        <div class="cv-skills-item">
          <h3 class="cv-skills-title">
            Programación
          </h3>
          <div class="cv-skills-description">
            <li class="skillset-item">
              PHP
            </li>
            <li class="skillset-item">
              OOP
            </li>
            <li class="skillset-item">
              Java
            </li>
            <li class="skillset-item">
              SQL
            </li>
          </div>
        </div>
        <div class="cv-skills-item">
          <h3 class="cv-skills-title">
            Diseño
          </h3>
          <div class="cv-skills-description">
            <li class="skillset-item">
              Photoshop
            </li>
            <li class="skillset-item">
              Illustrator
            </li>
            <li class="skillset-item">
              UX/UI
            </li>
            <li class="skillset-item">
              Responsive design
            </li>
          </div>
        </div>
        <div class="cv-skills-item">
          <h3 class="cv-skills-title">
            OS, Herramientas de desarrollo web
          </h3>
          <div class="cv-skills-description">
              <ul class="skillset">
                <li class="skillset-item">
                  Linux
                </li>
                <li class="skillset-item">
                  Mac OS X
                </li>
                <li class="skillset-item">
                  Windows
                </li>
                <li class="skillset-item">
                  Git
                </li>
                <li class="skillset-item">
                  GitHub / Bitbucket
                </li>
                <li class="skillset-item">
                  Trello / Jira Software
                </li>
              </ul>
          </div>
        </div>
      </section>
    </div>
  </article>

</main>



<?php include_once "../include/body-end.php" ?>
