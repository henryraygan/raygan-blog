import gulp from 'gulp';
import plumber from 'gulp-plumber';
import pug from 'gulp-pug';
import browserSync from 'browser-sync';
import sass from 'gulp-sass';
import postcss from 'gulp-postcss';
import cssnano from 'cssnano';
import watch from 'gulp-watch';
import browserify from 'browserify';
import babelify from 'babelify';
import source from 'vinyl-source-stream';
import sourcemaps from 'gulp-sourcemaps';
import buffer from 'vinyl-buffer';
import php2html from 'gulp-php2html';

const server = browserSync.create();




const config = {
  srcSass: "./dev/scss/v1/*.scss",
  srcTemplates: "./dev/php/v1/templates/**/*.php",
  destSass: "./dist/assets/css",
  destTemplates: "./dist/"
};


const postcssPlugins = [
  cssnano({
    core: false,
    autoprefixer: {
      add: true,
      browsers: '> 1%, last 2 versions, Firefox ESR, Opera 12.1'
    }
  })
];

const sassOptions = {
  // compressed
  // expanded
  outputStyle: 'compressed'
};



gulp.task('styles', () =>
  gulp.src(config.srcSass)
  .pipe(sourcemaps.init({
    loadMaps: true
  }))
  .pipe(plumber())
  .pipe(sass(sassOptions))
  .pipe(postcss(postcssPlugins))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(config.destSass))
  .pipe(server.stream({
    match: '**/*.css'
  }))
);

gulp.task('php2html', () =>
  gulp.src(config.srcTemplates)
  .pipe(php2html())
  .pipe(gulp.dest(config.destTemplates))
);



gulp.task('pug', () =>
  gulp.src('./dev/pug/pages/*.pug')
  .pipe(plumber())
  .pipe(pug())
  .pipe(gulp.dest('./dist'))
);



gulp.task('scripts', () =>
  browserify('./dev/js/index.js')
  .transform(babelify)
  .bundle()
  .on('error', function (err) {
    console.error(err);
    this.emit('end')
  })
  .pipe(source('scripts.js'))
  .pipe(buffer())
  .pipe(sourcemaps.init({
    loadMaps: true
  }))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('./dist/assets/js'))
);

gulp.task('default', () => {
  server.init({
    server: {
      baseDir: './dist'
    },
  });

  watch('./dev/scss/v1/**/*.scss', () => gulp.start('styles', server.reload));
  watch('./dev/js/**/*.js', () => gulp.start('scripts', server.reload));
  watch('./dev/pug/**/*.js', () => gulp.start('pug', server.reload));
  watch('./dev/php/v1/**/*.php', () => gulp.start('php2html', server.reload));

});
